#!/bin/bash

_usage() {
  echo "Usage: $0 CHECKNAME PROFILE"
  echo "Checks: infra, os, keys, vol"
  echo "you might want to output stdout to a .tsv file"
}


_infra () {
	for region in $(aws ec2 describe-regions --output text | cut -f3); do
	   aws ec2 describe-instances --output text --region $region --profile $2 --query 'Reservations[*].Instances[*].[InstanceId, InstanceType, State.Name, Placement.AvailabilityZone, PrivateIpAddress, PrivateDnsName, PublicIpAddress, PublicDnsName, [Tags[?Key==`Name`].Value] [0][0], [Tags[?Key==`environment`].Value] [0][0], [Tags[?Key==`team`].Value] [0][0] ]'
	done
}

_vol () {
	for region in $(aws ec2 describe-regions --output text | cut -f3); do
	   aws ec2 describe-volumes --region $region --output text --profile $2 --query 'Volumes[*].{ID:VolumeId,InstanceId:Attachments[0].InstanceId,AZ:AvailabilityZone,Size:Size}'
	done
}

_os () {
	for region in $(aws ec2 describe-regions --output text | cut -f3); do
	   aws ec2 describe-instances  --profile $2 --region $region --output text --query 'Reservations[*].Instances[*].[InstanceId,Platform]'
	done
}


_keys () {
	for region in $(aws ec2 describe-regions --output text | cut -f3); do
		echo "Region:$region";aws ec2 describe-key-pairs --output text --region $region --profile $2
	done
}



case "$1" in
        infra)
            _infra "$@"
            ;;

        vol)
            _vol "$@"
            ;;

        os)
            _os "$@"
            ;;
        keys)
            _keys "$@"
            ;;

        *)
            _usage
            exit 1

esac
